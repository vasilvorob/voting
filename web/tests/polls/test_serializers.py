import pytest
from polls.serializers import (ChoiceSerializer, CreateUpdatePollsSerializer,
                               QuestionSerializer, VotingSerializer, error_messages)
from polls.models import Choice, Question


def test_valid_choice_serializer():
    valid_serializer_data = {
        "title": "Trump"
    }
    serializer = ChoiceSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.errors == {}


def test_invalid_choice_serializer():
    invalid_serializer_data = {}
    serializer = ChoiceSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.errors == {"title": ["This field is required."]}


@pytest.mark.django_db
def test_create_poll_serializer():
    valid_data = {
        "question": "Who better ?",
        "choice": [
            "Dima", "Vova", "Masha"
        ]
    }
    serializer = CreateUpdatePollsSerializer(data=valid_data)
    assert serializer.is_valid()
    serializer.save()
    assert Choice.objects.count() == 3
    assert Question.objects.count() == 1

    invalid_data = {
        "question": "Who better ?",
        "choice": [
            "Dima"
        ]
    }
    serializer = CreateUpdatePollsSerializer(data=invalid_data)
    assert not serializer.is_valid()
    assert serializer.errors == {'choice': [error_messages['questions_number']]}


@pytest.mark.django_db
def test_update_poll_serializer(add_question):
    question = add_question(title="Who the best ?")
    valid_data = {
        "question": "Who the best ?",
        "choice": [
            "Dog", "Cat", "Snake"
        ]
    }
    serializer = CreateUpdatePollsSerializer(instance=question, data=valid_data)
    assert serializer.is_valid()
    serializer.save()
    assert serializer.validated_data == valid_data
    assert Choice.objects.count() == 3
    assert Question.objects.count() == 1
