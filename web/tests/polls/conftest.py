import pytest

from polls.models import Question, Choice


@pytest.fixture(scope='function')
def add_question():
    def _add_question(title):
        question = Question.objects.create(title=title)
        return question
    return _add_question
