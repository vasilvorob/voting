import pytest
from polls.models import Question, Choice


@pytest.mark.django_db
def test_question_model():
    question = Question(title="Who is stronger ?")
    question.save()
    assert question.title == "Who is stronger ?"
    assert question.created
    assert str(question) == question.title


@pytest.mark.django_db
def test_choice_model(add_question):
    question = add_question(title="Who is stronger ?")
    choice = Choice.objects.create(question=question, title="Putin")
    assert choice.title == "Putin"
    assert choice.question == question
