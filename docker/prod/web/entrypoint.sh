#!/bin/sh

python manage.py wait_for_db

python manage.py check --deploy

python manage.py collectstatic --no-input

python manage.py migrate --noinput


gunicorn src.wsgi:application --bind 0.0.0.0:8000

exec "$@"
